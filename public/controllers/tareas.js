function tareas() {
	this.tabla = this.load_table();
}

tareas.prototype.init = function() {
	this.addEventoRefresh();
};

tareas.prototype.addEventoRefresh = function(id_button) {
	var self = this;
	$('.'+id_button).on('click', function(event) {
		event.preventDefault();
		self.tabla.draw();
	});
};

tareas.prototype.load_table = function() {

	var oTable = $('#tareas_table').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": url_table,
		"columns": [
			{ data: 'id', name: 'tareas.id', type: 'num' },
			{ data: 'nombre', name: 'tareas.estado', type: 'string' },
			{ data: 'name', name: 'users.name', type: 'string' },
			{ data: 'estado', name: 'tareas.estado', type: 'html' },
			{ data: 'buttons', name: 'buttons', 'type': 'hmtl',"searchable": false, 'orderable':false }
		],
		"language": {
			"url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
		}
	});

	return oTable;
};