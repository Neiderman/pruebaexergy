<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Auth;
use App\User;
use App\Tareas;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = User::find(Auth::user()->id);
        if (!$usuario) {
            return $this->logout();
        }
        
        $ct_pendientes = Tareas::where(function($tareas) use ($usuario) {
            $tareas->where('user_id',$usuario->id);
            $tareas->where('estado','Pendiente');
        })->count();
        
        $ct_proceso = Tareas::where(function($tareas) use ($usuario) {
            $tareas->where('user_id',$usuario->id);
            $tareas->where('estado','En proceso');
        })->count();
        
        $ct_terminadas = Tareas::where(function($tareas) use ($usuario) {
            $tareas->where('user_id',$usuario->id);
            $tareas->where('estado','Terminado');
        })->count();
        
        $ct_canceladas = Tareas::where(function($tareas) use ($usuario) {
            $tareas->where('user_id',$usuario->id);
            $tareas->where('estado','Cancelado');
        })->count();
        
        return view('auth.cuenta')->with([
            'ct_pendientes' => $ct_pendientes,
            'ct_proceso' => $ct_proceso,
            'ct_terminadas' => $ct_terminadas,
            'ct_canceladas' => $ct_canceladas,
            'id' => $usuario->id,
            'nombre' => $usuario->name,
            'email' => $usuario->email,
            'descripcion' => $usuario->descripcion,
            'desde' => $usuario->created_at->toDateTimeString()
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'descripcion' => 'string|max:255',
        ])->validate();

        $usuario = User::find($request->id);
        if (!$usuario) {
            return $this->logout();
        }

        $usuario->name = $request->nombre;
        $usuario->email = $request->email;
        $usuario->descripcion = $request->descripcion;
        $usuario->update();

        Alert::success('Perfecto!', 'Se han actualizado tus datos.');
        return redirect('/cuenta');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        \Auth::guard()->logout();

        return redirect('/');
    }
}
