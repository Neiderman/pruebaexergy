<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class GraficasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('graficas')->with([
            'a_inicio' => 'active'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataGraph()
    {
        $data = [];
        $año = 2000;

        for ($i=0; $i <= 15; $i++) { 
            
            $dia = rand(1,28);
            $mes = rand(1,12);
            
            $fecha = intval(Carbon::createFromFormat('Y-m-d H:i:s', "$año-$mes-$dia 00:00:00")->timestamp.'000');
            $valor = rand(2000,200000);
            array_push($data, [$fecha,$valor]);
            $año = $año + 1;
        }

        return json_encode($data);
    }
}
