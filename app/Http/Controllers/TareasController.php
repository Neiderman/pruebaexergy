<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use App\Tareas;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;

class TareasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tareas.tareas');
    }

    public function create()
    {
        return view('tareas.crear');
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $tarea = $request->tarea;
        $estado = $request->estado;
        $user_id = \Auth::user()->id;

        $user = User::findOrFail(\Auth::user()->id);

        $tareas = new Tareas();
        $tareas->nombre = $tarea;
        $tareas->estado = $estado;
        $tareas->user_id = $user_id;
        $tareas->save();

        Alert::success('Perfecto!', 'Se ha creado la nueva tarea.');
        return redirect('/tareas');
    }


    public function edit($id)
    {
        $tarea = Tareas::find($id);
        if (!$tarea) {
            Alert::error('Ops!', 'No se ha encontrado la tarea buscada.');
            return redirect('/tareas');
        }

        if($tarea->estado == 'Pendiente') {
            $p = 'selected';
        }

        if($tarea->estado == 'En proceso') {
            $e = 'selected';
        }

        if($tarea->estado == 'Terminado') {
            $t = 'selected';
        }

        if($tarea->estado == 'Cancelado') {
            $c = 'selected';
        }

        return view('tareas.editar')->with([
            'id' => $tarea->id,
            'p' => isset($p) ? $p : '',
            'e' => isset($e) ? $e : '',
            't' => isset($t) ? $t : '',
            'c' => isset($c) ? $c : '',
            'mensaje' => $tarea->nombre
        ]);
    }

    public function update(Request $request)
    {
        $this->validator($request->all())->validate();
        $id = $request->id;
        $tarea = $request->tarea;
        $estado = $request->estado;

        $tareas = Tareas::find($id);
        if (!$tareas) {
            Alert::error('Ops!', 'No se ha encontrado la tarea buscada.');
            return redirect('/tareas');
        }

        $tareas->nombre = $tarea;
        $tareas->estado = $estado;
        $tareas->update();

        Alert::success('Perfecto!', 'Se ha actualizado la tarea.');
        return redirect('/tareas');
    }

    public function delete(Request $reques)
    {
        $tarea = Tareas::find($reques->id);
        if ($tarea) {
            $tarea->delete();
            return ['estado' => 'ok', 'mensaje' => 'Registro eliminado con exito.'];
        } else {
            return ['estado' => 'no', 'mensaje' => 'El registro no eliminado con exito por que no existe.'];
        }
    }

    public function data(Request $request)
    {
        $tareas = Tareas::select([
            'tareas.id as id',
            'tareas.nombre as nombre',
            'tareas.estado as estado',
            'users.name as name'
        ])
        ->join('users','tareas.user_id','=','users.id');

        return Datatables::of($tareas)
        ->editColumn('estado',function($tareas){
            $estado = $tareas->estado;
            $status = 'success';

            if ($estado == 'Terminado') {
                $status = 'success';
            } else if ($estado == 'Pendiente') {
                $status = 'info';
            } else if ($estado == 'En proceso') {
                $status = 'warning';
            } else if ($estado == 'Cancelado') {
                $status = 'danger';
            } else {
                $status = 'default';
            }

            return '<span class="label label-'.$status.'">'.$tareas->estado.'</span>';
        })
        ->addColumn('buttons',function($tareas){
            $id = $tareas->id;
            $b_editar = '<a href="'.route('e_tareas',$id).'" class="btn btn-info btn-xs btn-fill btn_editar" title="Editar esta tarea"><i class="ti-pencil"></i></a>';
            $b_eliminar = '<a class="btn btn-danger btn-xs btn-fill btn_eliminar" title="Eliminar esta tarea" onclick="fn_delete('.$id.');"><i class="ti-close"></i></a>';
            return $b_editar.$b_eliminar;
        })
        ->rawColumns(['estado','buttons'])
        ->make(true);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'tarea' => 'required|string|max:255',
            'estado' => 'required|string'
        ]);
    }
}
