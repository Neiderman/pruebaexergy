<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{

	protected $fillable = [
		'id',
		'nombre',
		'estado',
		'user_id',
	];

	protected $table = 'tareas';
}
