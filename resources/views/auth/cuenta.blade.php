@extends('layouts.layout')

@section('titulo_m')
Gestión de cuenta
@endsection

@section('cuenta')
active
@endsection

@section('custom_styles')
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Editar perfil</h4>
                    </div>
                    <div class="content">
                        <form method="POST" action="{{ action('UserController@update') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="id" value="{{ isset($id) ? $id : '' }}">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                                        <label>Nombre(s) y apellidos</label>
                                        <input type="text" class="form-control border-input" id="nombre" name="nombre" placeholder="Nombre(s) y apellidos" value="{{ isset($nombre) ? $nombre : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="exampleInputEmail1">Correo electrónico</label>
                                        <input type="email" class="form-control border-input" placeholder="Email" id="email" name="email" value="{{ isset($email) ? $email : '' }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                                        <label>Sobre mi <small>Max. 255 caracteres</small></label>
                                        <textarea rows="5" class="form-control border-input" id="descripcion" name="descripcion" placeholder="Cuentanos un poco mas sobre ti..." value="" minlength="0" maxlength="255">{{ isset($descripcion) ? $descripcion : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Actualizar perfil</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="assets/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="assets/img/faces/face-0.jpg" alt="..."/>
                            <h4 class="title">{{ isset($nombre) ? $nombre : '' }}<br />
                                <a href="mailto:{{ isset($email) ? $email : '' }}" target="_blank"><small>{{ isset($email) ? $email : '' }}</small></a>
                            </h4>
                            <p class="description text-center">
                                "{{ isset($descripcion) ? $descripcion : '' }}"
                            </p>
                            <small>
                                Miembro desde<br> 
                                {{ isset($desde) ? $desde : '' }}
                            </small>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>{{ isset($ct_pendientes) ? $ct_pendientes : 0 }}<br /><small>Tareas pendientes</small></h5>
                            </div>
                            <div class="col-md-3">
                                <h5>{{ isset($ct_proceso) ? $ct_proceso : 0 }}<br /><small>Tareas en proceso</small></h5>
                            </div>
                            <div class="col-md-3">
                                <h5>{{ isset($ct_terminadas) ? $ct_terminadas : 0 }}<br /><small>Tareas terminadas</small></h5>
                            </div>
                            <div class="col-md-3">
                                <h5>{{ isset($ct_canceladas) ? $ct_canceladas : 0 }}<br /><small>Tareas canceladas</small></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

@section('custom_js')
@endsection