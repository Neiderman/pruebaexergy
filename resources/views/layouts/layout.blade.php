<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />


	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet"/>
	<link href="{{ asset('assets/css/paper-dashboard.css') }}" rel="stylesheet"/>
	<link href="{{ asset('assets/css/demo.css') }}" rel="stylesheet" />

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>

	<link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet">

	@yield('custom_styles')

</head>
<body>

	<div class="wrapper">
		<div class="sidebar" data-background-color="white" data-active-color="danger">

			<div class="sidebar-wrapper">
				<div class="logo">
					@guest
					<a href="{{ url('/') }}" class="simple-text">
						{{ config('app.name', 'Laravel') }}
					</a>
					@else
					<a href="{{ url('/home') }}" class="simple-text">
						{{ config('app.name', 'Laravel') }}
					</a>
					@endguest
				</div>

				<ul class="nav">
					@guest
					@else
					<li class="@yield('inicio')">
						<a href="{{ url('/home') }}">
							<i class="ti-home"></i>
							<p>Inicio</p>
						</a>
					</li>
					<li class="@yield('grafica')">
						<a href="{{ url('/graficas') }}">
							<i class="ti-panel"></i>
							<p>Graficar</p>
						</a>
					</li>
					<li class="@yield('tareas')">
						<a href="{{ url('/tareas') }}">
							<i class="ti-list"></i>
							<p>Gestión tareas</p>
						</a>
					</li>
					<li class="active-pro @yield('cuenta')">
						<a href="{{ url('cuenta') }}">
							<i class="ti-user"></i>
							<p>Mi cuenta</p>
						</a>
					</li>
					@endguest
				</ul>
			</div>
		</div>

		<div class="main-panel">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar bar1"></span>
							<span class="icon-bar bar2"></span>
							<span class="icon-bar bar3"></span>
						</button>
						<a class="navbar-brand">@yield('titulo_m')</a>
					</div>
					<div class="collapse navbar-collapse">
						@guest
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="{{ url('login') }}">
									<i class="ti-user"></i>
									<p>Login</p>
								</a>
							</li>
							<li>
								<a href="{{ url('register') }}">
									<i class="ti-plus"></i><i class="ti-user"></i>
									<p>Registro</p>
								</a>
							</li>
						</ul>
						@else
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a>
									<p>{{ Auth::user()->name }}</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="ti-settings"></i>
									<p>Opciones</p>
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="{{ action('UserController@index') }}">Mi cuenta</a></li>
									<li><a href="{{ url('logout') }}">Cerrar sesión</a></li>
								</ul>
							</li>
						</ul>
						@endguest
					</div>
				</div>
			</nav>

			@yield('content')


			<footer class="footer">
				<div class="container-fluid">
					<div class="copyright pull-right">
						&copy; {{ date('Y') }}. Por <a href="http://www.fb.com/neiderxd16" target="_blank">Esneider Mejia Ciro</a>
					</div>
				</div>
			</footer>

		</div>
	</div>
</body>

<script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap-checkbox-radio.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('assets/js/paper-dashboard.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

@include('sweetalert::alert')

@yield('custom_js')


</html>
