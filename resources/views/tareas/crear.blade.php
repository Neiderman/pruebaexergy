@extends('layouts.layout')

@section('titulo_m')
Gestión de tareas > Crear
@endsection

@section('tareas')
active
@endsection

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/b-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="col-lg-2 col-md-7"></div>
        <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h4 class="title">Crear una tarea</h4>
                </div>
                <div class="content">
                    <form method="POST" action="{{ action('TareasController@store') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                                    <label>Estado</label>
                                    <select class="form-control border-input" id="estado" name="estado" required>
                                        <option value="">Seleccione un estado...</option>
                                        <option value="Pendiente" selected>Pendiente</option>
                                        <option value="En proceso">En proceso</option>
                                        <option value="Terminado">Terminado</option>
                                        <option value="Cancelado">Cancelado</option>
                                    </select>

                                    @if ($errors->has('estado'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('tarea') ? ' has-error' : '' }}">
                                    <label>Tarea</label>
                                    <small>Max. 255 caracteres</small>
                                    <textarea rows="5" class="form-control border-input" name="tarea" id="tarea" placeholder="Acción o tarea a almacenar." minlength="3" maxlength="255" autofocus required></textarea>

                                    @if ($errors->has('tarea'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tarea') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('/tareas') }}" class="btn btn-danger btn-fill btn-wd">Cancelar</a>
                            <button type="submit" class="btn btn-success btn-fill btn-wd">Guardar</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
@endsection