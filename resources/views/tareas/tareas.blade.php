@extends('layouts.layout')

@section('titulo_m')
Gestión de tareas
@endsection

@section('tareas')
active
@endsection

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/b-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Gestión de tareas 
                            <div class="pull-right">
                                <a href="{{ url('/tareas/crear') }}" class="btn btn-success btn-xs btn-fill" title="Agrega una nueva tarea"><i class="ti-plus"></i></a>
                                <button class="btn btn-info btn-xs btn-fill reload_table" title="Actualiza la lista de tareas"><i class="ti-reload"></i></button>
                            </div>
                        </h4>
                        <p class="category">Esta punto es el crud que solicitan para hacer una gestión de tareas. Aqui se permite realizar filtros y cambios, es decir, puedes crear, actualizar, editar y eliminar tareas.</p>
                    </div>
                    <div class="content">
                        <div class="content table-responsive table-full-width">
                            <table id="tareas_table" class="table table-striped">
                                <thead>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Propietario</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Propietario</th>
                                    <th>Estado</th>
                                    <th></th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/b-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
<script>
    var url_table = "{{ action('TareasController@data') }}";
    var url_delete = "{{ action('TareasController@delete') }}";
</script>
<script src="{{ asset('controllers/tareas.js') }}"></script>
<script>
    var tareas = new tareas();
    tareas.addEventoRefresh('reload_table');
    
    function fn_delete(id) {
        $.ajax({
            url: url_delete,
            type: 'POST',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: id,
            },
        })
        .done(function(res) {
            if(res.estado == 'ok'){
                swal("Perfecto!", "Se ha eliminado esta tarea con éxito!.", "success");
                $('.reload_table').click();
            } else {
                swal("Opsss!", res.mensaje, "error");
            }
        })
        .fail(function() {
            console.log("error");
        });
        
    }
</script>
@endsection