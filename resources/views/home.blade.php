@extends('layouts.layout')

@section('titulo_m')
Inicio
@endsection

@section('inicio')
active
@endsection

@section('custom_styles')
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Bienvenidos a PrExergy</h4>
                        <p class="category">Esta prueba es creada con el fin de presentar solución a la siguiente petición.</p>
                    </div>
                    <div class="content">
                        <p>
                            Prueba para programador Laravel

                            La prueba consiste en crear una página web, usando el framework Laravel, en la cual un usuario puede
                            iniciar sesión o puede crear una cuenta nueva. Se debe configurar una base de datos usando mysql para
                            guardar la información de los usuarios. Una vez el usuario inicia sesión debe existir un menú el cual tendrá
                            una opción llamada “Graficar”. Esta opción direcciona al usuario a una página que gráfica información
                            aleatoria de una serie de tiempo usando la librería Highcharts. El menú debe contar con una segunda
                            opción llamada “Gestionar Tareas”. Esta redireccionará al usuario a una vista con un CRUD para “Tareas”,
                            estas consisten en un título y una descripción. Queda a decisión del aspirante como presentar todas estas
                            funcionalidades. El Proyecto se debe subir a un repositorio (Github, Bitbucket, Gitlab) y se debe enviar el
                            enlace para su descarga al correo <a href="mailto:info@exergy-ma.co">info@exergy-ma.co</a>.
                            Para la calificación se tendrán en cuenta aspectos como:

                            <ul>
                                <li>Manejo del framework</li>
                                <li>Validaciones en el registro de un nuevo usuario</li>
                                <li>Creatividad de diseño</li>
                                <li>Orden del código</li>
                                <li>Enrutamiento</li>
                                <li>Uso de librerías</li>
                            </ul>
                        </p>
                        <div class="footer">
                            <hr>
                            <div class="stats">
                                <i class="ti-timer"></i> {{ date('d/m/Y H:i') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
@endsection