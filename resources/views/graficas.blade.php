@extends('layouts.layout')

@section('titulo_m')
Graficar
@endsection

@section('grafica')
active
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Graficar <button class="btn btn-info btn-xs btn-fill" id="btn_reload_graph"><i class="ti-reload"></i></button></h4>
                        <p class="category">Esta gráfica (serie de tiempo) se muestra con información aleatoria, generada desde el controlador GraficasController.php, el método creado se llama getDataGraph, este retorna un Json especial para HighCharts.</p>
                    </div>
                    <div class="content">
                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script src="{{ asset('controllers/graficas.js') }}"></script>
<script>
    var url_data = "{{ action('GraficasController@getDataGraph') }}";
    $(document).ready(function() {
        var graphs = new graficas();
        graphs.addEventoReload('btn_reload_graph');
    });
</script>
@endsection