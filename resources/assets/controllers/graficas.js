function graficas() {
	this.load_graph();
}

graficas.prototype.addEventoReload = function(id_button) {
	var self = this;
	$('#'+id_button).on('click', function(event) {
		event.preventDefault();
		self.load_graph();
	});
};

graficas.prototype.load_graph = function() {
	$.ajax({
		url: url_data,
		dataType: 'json',
	})
	.done(function(res) {
		var data = res;
		Highcharts.chart('container', {
			chart: {
				zoomType: 'x'
			},
			title: {
				text: 'Linea de tiempo por ventas anuales'
			},
			subtitle: {
				text: 'Estos valores son aleatorios'
			},
			xAxis: {
				type: 'datetime'
			},
			yAxis: {
				title: {
					text: 'Valor'
				}
			},
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
						]
					},
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},

			series: [{
				type: 'area',
				name: 'Cantidad total de ventas',
				data: data
			}]
		});

	})
	.fail(function() {
		console.log("error");
	});
};