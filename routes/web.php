<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PrincipalController@index')->name('inicio');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'UserController@logout')->name('logout');

Route::get('/graficas', 'GraficasController@index')->name('graficas');
Route::get('/graficar', 'GraficasController@getDataGraph')->name('graficar');

Route::get('/tareas', 'TareasController@index')->name('tareas');

Route::get('/tareas/crear', 'TareasController@create')->name('n_tareas');
Route::post('/tareas/crear/store', 'TareasController@store')->name('s_tarea');

Route::get('/tareas/data', 'TareasController@data')->name('tareas_data');
Route::get('/tareas/editar/{id}', 'TareasController@edit')->name('e_tareas');
Route::post('/tareas/editar/update', 'TareasController@update')->name('u_tareas');
Route::post('/tareas/eliminar', 'TareasController@delete')->name('d_tareas');

Route::get('/cuenta', 'UserController@index')->name('cuenta');
Route::post('/cuenta/actualizar', 'UserController@update')->name('u_cuenta');